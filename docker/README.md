# Creating and Running Docker Images of the ROS/Gazebo New England Shelf Simulator

2021-11-15  MVJ 

As of 2021-11-15 this document describes only how to create and run images on a local machine.  The intention is ultimately to run containerized simulations in AWS robomaker.

These instructions worked on an ubuntu 18.04 machine with an Nvidia Quadro K1200.

## Getting this document

This document is part of the wind_config public repository on bitbucket.  You will need other resources in that repository to follow the directions below.  Clone the repo onto your local machine:

    git clone https://bitbucket.org/wind_energy/wind_config

Change to the directory containing this file.  Then close this copy and open the copy from the repository in case there have been updates.

    cd wind_config/docker/

## Setting up docker on your machine

    sudo apt-get install docker.io
    sudo usermod -a -G docker $USERNAME  # set permissions
    sudo service docker start 
    docker info  # test

More info here if needed: https://docs.docker.com/install/

## Setting up rocker

Follow the instructions here to set up the NVIDIA Container Toolkit: https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker

The page includes instructions for installing docker.  I believe I followed the instructions in the previous block for installing docker.io via apt, but if this doesn't work, try reinstalling docker using the method recommended on the NVIDIA Container Toolkit page: https://docs.docker.com/engine/install/ubuntu/

Either way, the summary steps for the NVIDIA Container Toolkit are:

    distribution=$(. /etc/os-release;echo $ID$VERSION_ID) \
    && curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add - \
    && curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

    sudo apt-get update
    sudo apt-get install -y nvidia-docker2
    sudo systemctl restart docker

Test (expected output appears at the link above and will take some time to download the image):

    sudo docker run --rm --gpus all nvidia/cuda:11.0-base nvidia-smi

## Creating the docker images

There is a Dockerfile for the base image (mostly osrf/ros:melodic-desktop-full with some additional gazebo packages plus some additional miscellaneous packages and utilities), and a docker file specific to the NES simulator.

Build the base image:

    cd ./base/
    docker build -t base .  # build it - takes several min

Build the simulator image:

    cd ../sim/
    docker build -t sim .

Building the simulator image will create a ROS workspace populate it with code from github and bitbucket and compile it.  Building the image will also compile some netCDF libraries needed by the ocean_model_interfaces library (for importing NECOFS model output).  All source is open and does not require credentials.

## Running the Simulation

To run the gazebo GUI you need to provide the image with access to your display.  There are various ways to do this.  I was successful only with rocker.

    sudo apt install python3-rocker

Launch files in the workspace point to directories in which certain gazebo plugins expect to find pre-processed bathymetric tiles and a database of NECOFS FVCOM output.  These are available as a directory on Dropbox: https://www.dropbox.com/sh/ldbxv9bgxn045vq/AABHOrWx55Xu5KaPsy9-P7dPa?dl=0.  Download and unzip the directory onto your local machine or else use a Dropbox client (and ask mjakuba@whoi.edu to share the directory).  The line below (the "~/Dropbox/sim_resources/" part) should be modified to point to the location of this directory on your machine.  The size of this directory is 19 GB (2021-11-15) and will grow as we add detail to the simulation.
   
    rocker --volume ~/Dropbox/sim_resources/:/home/robomaker/tmp/ --x11 --nvidia --user-override-name robomaker sim

This should log you into the image and you should see a prompt.  The intention behind the use of user-override-name is to log in to the image as user robomaker and trigger environment configuration.   As of 2021-11-15, this does not work.  But you can configure your environment as follows:

    source ./devel/setup.bash
    export GAZEBO_MODEL_PATH=/home/robomaker/tmp/ncei_bathy_tiles/

Now start the sim:

    roslaunch wind_config sim.launch
    
A gazebo window should appear.  On the left-hand pane, click the Models drop-down, right-click on lrauv, and select "follow."  Now click play on the bottom pane.  Bathymetry should appear and the vehicle should start driving a mission.

### Additional reading and resources:

[Running a container in Robomaker](https://aws.amazon.com/blogs/robotics/run-any-high-fidelity-simulation-in-aws-robomaker-with-gpu-and-container-support/)

[AWS Robomaker container requirements](https://docs.aws.amazon.com/robomaker/latest/dg/container-requirements.html)


[AWS robomaker developer guide]
